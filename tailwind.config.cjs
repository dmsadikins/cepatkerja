/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors");
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {
            fontFamily: {
                sans: ["Inter", "Helvetica", "sans-serif"],
            },
            fontSize: {
                base: "0.813rem",
                xl: "1rem",
                "2xl": "1.625rem",
                "3xl": "2.438rem",
                "4xl": "3.25rem",
                "5xl": "4.063rem",
            },
            colors: {
                "btn-shadow": "rgba(113, 121, 136, 0.1)",
                "dashed-color": "rgba(var(--border-color), <alpha-value>)",
                "sidebar-dark": "rgba(var(--sidebar-dark), <alpha-value>)",
                "sidebar-heading":
                    "rgba(var(--sidebar-heading), <alpha-value>)",
                "sidebar-icon": "rgba(var(--sidebar-icon), <alpha-value> )",
                "body-color": "rgba(var(--bg-body), <alpha-value> )",
                muted: "rgba(var(--gray-500), <alpha-value>)",
                "muted-dark": "rgba(var(--gray-dark-500), <alpha-value>)",
                primary: "rgba(var(--primary), <alpha-value>)",
                "primary-active": "rgba(var(--primary-active), <alpha-value>)",
                "primary-light": "rgba(var(--primary-light), <alpha-value>)",
                "primary-light-dark":
                    "rgba(var(--primary-light-dark), <alpha-value>)",
                secondary: "rgba(var(--gray-300), <alpha-value>)",
                "secondary-dark": "rgba(var(--gray-300-dark), <alpha-value>)",
                "secondary-active": "rgba(var(--gray-400), <alpha-value>)",
                "secondary-active-dark":
                    "rgba(var(--gray-400-dark), <alpha-value>)",
                "secondary-light": "rgba(var(--gray-100), <alpha-value>)",
                "secondary-light-dark":
                    "rgba(var(--gray-100-dark), <alpha-value>)",
                "secondary-inverse": "rgba(var(--gray-800), <alpha-value>)",
                "secondary-inverse-dark":
                    "rgba(var(--gray-800-dark), <alpha-value>)",
                success: "rgba(var(--success), <alpha-value>)",
                "success-active": "rgba(var(--success-active), <alpha-value>)",
                "success-light": "rgba(var(--success-light), <alpha-value>)",
                "success-light-dark":
                    "rgba(var(--success-light-dark), <alpha-value>)",
                "success-inverse": "white",
                info: "rgba(var(--info), <alpha-value>)",
                "info-active": "rgba(var(--info-active), <alpha-value>)",
                "info-light": "rgba(var(--info-light), <alpha-value>)",
                "info-light-dark":
                    "rgba(var(--info-light-dark), <alpha-value>)",
                warning: "rgba(var(--warning), <alpha-value>)",
                "warning-active": "rgba(var(--warning-active), <alpha-value>)",
                "warning-light": "rgba(var(--warning-light), <alpha-value>)",
                "warning-light-dark":
                    "rgba(var(--warning-light-dark), <alpha-value>)",
                danger: "rgba(var(--danger), <alpha-value>)",
                "danger-active": "rgba(var(--danger-active), <alpha-value>)",
                "danger-light": "rgba(var(--danger-light), <alpha-value>)",
                "danger-light-dark":
                    "rgba(var(--danger-light-dark), <alpha-value>)",

                light: "rgba(var(--gray-100), <alpha-value>)",
                "light-dark": "rgba(var(--gray-200-dark), <alpha-value>)",
                "light-active": "rgba(var(--gray-200), <alpha-value>)",
                "light-active-dark":
                    "rgba(var(--gray-300-dark), <alpha-value>)",
                "light-inverse": "rgba(var(--gray-600), <alpha-value>)",
                "light-inverse-dark":
                    "rgba(var(--gray-600-dark), <alpha-value>)",
            },
            boxShadow: {
                custom: "0 0 28px 0 rgba(82, 63, 105, 0.05)",
            },
        },
    },
    plugins: [],
};
