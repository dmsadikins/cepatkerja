import React from "react";
import clsx from "clsx";

const Button = ({ children, text, type = "button", bg, size, props }) => {
    return (
        <>
            <button
                className={clsx(
                    bg,
                    size,
                    "rounded-md text-white border-none py-3 px-6 "
                )}
                {...props}
                type={type}
            >
                {text || children}
            </button>
        </>
    );
};

export default Button;
