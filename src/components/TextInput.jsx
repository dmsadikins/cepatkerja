import React from "react";
import clsx from "clsx";

const TextInput = ({
    type = "text",
    placeholder = "",
    classes,
    label,
    ...props
}) => {
    return (
        <div className="relative">
            <input
                {...props}
                type={type}
                className={clsx(
                    classes,
                    "peer placeholder-transparent focus:outline-none p-3 w-full bg-white border border-gray-200 rounded-md focus:border focus:border-primary"
                )}
                placeholder={placeholder}
            />
            <label
                htmlFor={label}
                className={clsx(
                    "peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400, peer-placeholder-shown:top-3 ",
                    "peer-focus:-top-2.5 peer-focus:text-primary peer-focus:bg-white peer-focus:font-semibold ",
                    "absolute left-4 -top-2.5 bg-white text-gray-300 text-sm transition-all "
                )}
            >
                {label}
            </label>
        </div>
    );
};

export default TextInput;
