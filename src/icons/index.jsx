import Account from "./Account.jsx";
import Authentication from "./Authentication.jsx";
import Corporate from "./Corporate.jsx";
import DoubleArrow from "./DoubleArrow.jsx";
import UserBook from "./UserBook.jsx";
import Dashboard from "./Dashboard.jsx";

const Icons = () => {
    return (
        <>
            <div></div>
        </>
    );
};

export default {
    Account,
    Authentication,
    Corporate,
    Dashboard,
    DoubleArrow,
    UserBook,
};
