import React, { useState } from "react";
import icons from "../../icons";
import { motion } from "framer-motion";
import clsx from "clsx";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const Sidebar = ({ children, title, img, alt, icon, heading, footer }) => {
    const [selected, setSelected] = useState(0);
    const [expanded, setExpanded] = useState(true);

    const navigate = useNavigate();

    console.log(window.innerWidth);
    console.log(expanded);

    const handleSidebarClick = () => {
        setExpanded(!expanded);
    };

    const menu = [
        { title: "Dashboard", icon: icons.Dashboard },
        { title: "User", icon: icons.Dashboard },
    ];

    const handleLogout = () => {
        Cookies.remove("token");
        navigate("/login");
        Swal.fire({
            text: "You has been Log out!",
            icon: "success",
            timer: 2000,
            confirmButtonText: "OK",
        });
    };

    return (
        <aside
            className="shrink-0 flex-column relative bg-sidebar-dark md:w-[275px] border-r-none min-h-[100vh] ease-in-out duration-500"
            style={expanded ? { width: "275px" } : { width: "75px" }}
        >
            <div className="flex items-center justify-start gap-[0.5] md:gap-1 border-dashed border-b relative border-slate-600  px-4  h-[70px] ">
                <div className="flex items-center">
                    {<img src={img} alt={alt} />}
                    <h5
                        className="text-white font-extrabold uppercase collapse md:visible ease-in-out duration-200"
                        style={
                            expanded
                                ? { visibility: "visible" }
                                : { visibility: "collapse" }
                        }
                    >
                        {title}
                    </h5>
                </div>

                <button
                    className="bg-white p-2 text-sm rounded-md text-muted drop-shadow-md absolute top-50 right-[-15px]"
                    onClick={handleSidebarClick}
                    style={
                        expanded
                            ? { transform: "rotate(180deg)" }
                            : { transform: "rotate(0deg)" }
                    }
                >
                    <icons.DoubleArrow />
                </button>
            </div>
            <div className="flex-column overflow-hidden">
                <div className="items-center py-[0.65rem] px-1-rem my-2 overflow-auto hover:overflow-scroll">
                    <div className="px-3">
                        <Link
                            to="/dashboard"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Dashboard />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Dashboard
                            </span>
                        </Link>

                        <div className="collapse px-[1rem] md:pt-5">
                            <div
                                className="collapse md:visible text-sidebar-heading uppercase font-semibold ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Management
                            </div>
                        </div>

                        <Link
                            to="/dashboard/user"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Authentication />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Management User
                            </span>
                        </Link>
                        <Link
                            to="/dashboard/profile"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Authentication />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Profile
                            </span>
                        </Link>

                        <Link
                            to="/dashboard/list-job-vacancy"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Account />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Job Listing
                            </span>
                        </Link>
                        <Link
                            to="/dashboard/list-job-vacancy/form"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Corporate />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Add New Job
                            </span>
                        </Link>
                        <Link
                            to="/dashboard/change-password"
                            className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                        >
                            <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                <icons.Dashboard />
                            </span>
                            <span
                                className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                Change Password
                            </span>
                        </Link>
                        <div className="collapse px-[1rem] md:pt-5">
                            <div
                                className="collapse md:visible text-sidebar-heading uppercase font-semibold ease-in-out duration-200"
                                style={
                                    expanded
                                        ? { visibility: "visible" }
                                        : { visibility: "collapse" }
                                }
                            >
                                EXIT
                            </div>
                        </div>

                        {Cookies.get("token") && (
                            <div
                                onClick={handleLogout}
                                className="flex justify-center items-center outline-none py-[0.65rem] px-[1rem] cursor-pointer"
                            >
                                <span className="flex items-center justify-center w-[2rem] mr-[0.5rem] text-sidebar-icon">
                                    <icons.Dashboard />
                                </span>
                                <span
                                    className="collapse md:visible md:flex md:grow items-center text-muted hover:text-white ease-in-out duration-200"
                                    style={
                                        expanded
                                            ? { visibility: "visible" }
                                            : { visibility: "collapse" }
                                    }
                                >
                                    Logout
                                </span>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <div className="absolute bottom-5 mx-7 text-white">{footer}</div>;
        </aside>
    );
};

export default Sidebar;
