import React from "react";

const Footer = () => {
    return (
        <div className="flex justify-between items-center bg-white text-muted h-[60px] px-6 shadow-custom">
            <div className="text-xs">
                2023 @ <span className="!text-gray-900">Cepatkerja.com</span>
            </div>
            <div>
                <ul className="collapse md:visible flex gap-4 list-none">
                    <li>About</li>
                    <li>Support</li>
                    <li>Purchase</li>
                </ul>
            </div>
        </div>
    );
};

export default Footer;
