import React from "react";
import icons from "../../icons";
import Button from "../../components/Button";
import Footer from "./Footer";
import Breadcrumb from "./Breadcrumb";
import Sidebar from "./Sidebar.jsx";
import Header from "./Header.jsx";

function index({ children }) {
    return (
        <section className="flex m-0 p-0">
            <Sidebar title={"Cepatkerja.com"} img="/vite.svg" />

            <div className="flex-column justify-between w-[100%] min-h-screen bg-gray-900">
                <div className="content-wrapper">
                    <Header />
                    <main className="bg-body-color  px-[5%] m-auto py-7 min-h-[85vh]">
                        <Breadcrumb />
                        {children}
                    </main>
                </div>
                <Footer />
            </div>
        </section>
    );
}

export default index;
