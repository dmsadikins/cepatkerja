import React from "react";

const Breadcrumb = () => {
    return (
        <div className="flex-column md:flex md:justify-between mb-3 ">
            <h1 className="text-gray-900 text-xl font-semibold">
                Multipurpose
            </h1>
            <ul className="flex gap-1">
                <li className="text-muted">Home</li>
                <li className="text-muted"> / </li>
                <li className="text-primary">Dashboard</li>
            </ul>
        </div>
    );
};

export default Breadcrumb;
