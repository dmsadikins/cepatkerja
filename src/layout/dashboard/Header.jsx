import React from "react";

const Header = () => {
    return (
        <>
            <div className="flex grow justify-between items-center px-6 h-[70px] bg-white shadow-custom">
                <div></div>
                <div className="flex items-center gap-2">
                    <h5>Logout</h5>
                    <img
                        className="p-2 rounded shadow-none hover:shadow-slate-400/50 hover:shadow-sm"
                        src="/react.svg"
                        alt="logo"
                    />
                </div>
            </div>
        </>
    );
};

export default Header;
