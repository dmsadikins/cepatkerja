import React from "react";
import Button from "../../components/Button";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

function Navbar() {
    return (
        <section className="mb-5 px-3 2xl:pt-5">
            <div className="container m-auto py-5 flex justify-between items-center">
                <div className="flex-auto w-50 md:w-10 2xl:40">
                    <img src="/vite.svg" alt="logo" width="40px" />
                </div>
                <div className="flex-auto justify-between xl:justify-end md:w-90 2xl:w-60 ">
                    <nav className="flex justify-between collapse md:visible items-center cursor-pointer">
                        <ul className="flex md:gap-3 lg:gap-10  text-xl">
                            <li className="text-primary !font-bold hover:text-primary-active">
                                Job
                            </li>
                            <li className="hover:text-primary ">Companies</li>
                            <li className="hover:text-primary">About</li>
                            <li className="hover:text-primary">Contact</li>
                        </ul>
                        <ul className="flex justify-between gap-10 text-xl">
                            <li className="hover:text-primary">
                                {Cookies.get("token") && (
                                    <Link
                                        to="/dashboard"
                                        className="relative flex gap-2 text-white text-semibold hover:drop-shadow bg-primary px-3 py-3 rounded-full"
                                        title="go to dashboard"
                                    >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth={1.5}
                                            stroke="currentColor"
                                            className="w-6 h-6"
                                        >
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                d="M5.636 5.636a9 9 0 1012.728 0M12 3v9"
                                            />
                                        </svg>

                                        <span className="absolute collapse peer-hover:visible">
                                            Dashboard
                                        </span>
                                    </Link>
                                )}
                                {!Cookies.get("token") && (
                                    <Link to="/login">Sign in</Link>
                                )}
                            </li>
                            <li>
                                {!Cookies.get("token") && (
                                    <Link
                                        to="/register"
                                        className="bg-primary hover:bg-primary-active py-3 px-6 rounded-md text-white"
                                    >
                                        Create Account
                                    </Link>
                                )}
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </section>
    );
}

export default Navbar;
