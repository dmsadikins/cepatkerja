import React from "react";
import Navbar from "./Navbar";
import Hero from "./Hero";

const index = ({ children }) => {
    return (
        <>
            <Navbar></Navbar>
            <Hero></Hero>
            <main>
                <div className="container m-auto">{children}</div>
            </main>
        </>
    );
};

export default index;
