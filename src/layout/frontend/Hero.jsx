import React from "react";
import Button from "../../components/Button";
import clsx from "clsx";

const Hero = () => {
    return (
        <section className="md:h-[90vh] px-3 2xl:pt-7">
            <div className="container m-auto flex flex-col-reverse md:flex-row">
                <div className="w-full md:w-[40%] lg:pt-10">
                    <h1
                        className={clsx(
                            "w-full text-center text-sidebar-dark font-bold",
                            "md:text-left",
                            "md:text-xl",
                            "lg:text-3xl",
                            "xl:text-3xl",
                            "2xl:text-4xl"
                        )}
                    >
                        Jump-Start Your
                        <span className="text-orange-400"> Career</span>
                    </h1>
                    <h3
                        className={clsx(
                            "w-full text-center font-bold ",
                            "md:text-left md:text-3xl md:mb-5",
                            "lg:text-2xl",
                            "xl:text-3xl",
                            "2xl:text-4xl"
                        )}
                    >
                        The Home Of Your
                    </h3>
                    <div className="w-full text-center md:text-left md:w-[30%] py-2">
                        <span className=" before:block before:absolute before:-inset-1 before:left-10 before:bg-orange-400 relative inline-block  before:rounded-lg">
                            <button className="relative bg-primary hover:bg-primary-active py-3 px-6 text-white rounded-lg block w-[280px]">
                                <span className="md:text-3xl font-bold">
                                    Dream Job
                                </span>
                            </button>
                        </span>
                    </div>

                    <h6
                        className={clsx(
                            "w-full text-center mt-3 mb-8 text-gray-600 text-semibold",
                            "md:text-left md:mt-5",
                            "2xl:text-xl"
                        )}
                    >
                        Getting the job of your dreams has never been easier if
                        <br className="sm:!collapse lg:visible" />
                        you need to, you can browse a job and apply.
                    </h6>

                    <div
                        className={clsx(
                            "duration-500 ease-linear bg-white shadow-lg shadow-slate-200  px-3 py-2 rounded-lg",
                            "md:inline-block md:mt-10"
                        )}
                    >
                        <div className="flex justify-between gap-3 items-center">
                            <div className="flex items-center hover:mr-40 ease-in-out duration-500">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="w-5 h-5 text-muted"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                                    />
                                </svg>

                                <input
                                    placeholder="Job Title"
                                    className="w-full md:w-60 text-xl py-3 px-2 rounded-md focus:outline-none placeholder:italic italic text-gray-400"
                                />
                            </div>
                            <div className="flex items-center gap-2 text-muted">
                                <div className="flex items-center gap-1">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418"
                                        />
                                    </svg>

                                    <select className="bg-transparent">
                                        <option>Indonesia</option>
                                        <option>Malaysia</option>
                                    </select>
                                </div>

                                <div className="bg-primary hover:bg-primary-active p-3 rounded-md">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6 text-white font-bold"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                                        />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-full md:w-[60%] 2xl:pt-30">
                    <img
                        src="/hero_image.png"
                        alt="hero_image"
                        className="w-full"
                    />
                </div>
            </div>
        </section>
    );
};

export default Hero;
