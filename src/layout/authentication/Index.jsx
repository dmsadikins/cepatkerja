import React from "react";

const Index = ({ children }) => {
    return (
        <div className="bg-body-color min-h-[100vh] flex justify-center items-center min-w-full mx-auto">
            <div className="flex-column justify-center">
                <div className="w-[500px]  p-10 rounded-md">{children}</div>
            </div>
        </div>
    );
};

export default Index;
