import React from "react";
import Dashboard from "./pages/admin/Dashboard";
import User from "./pages/admin/User";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import ChangePassword from "./pages/auth/ChangePassword";
import NotFound from "./pages/error/NotFound";
import Home from "./pages/landing/Home";
import JobVacancy from "./pages/admin/Job/JobVacancy";
import DataForm from "./pages/admin/Job/DataForm";
import Profile from "./pages/admin/Profile";
import Cookies from "js-cookie";

function App() {
    const PrivateRoutes = (props) => {
        if (Cookies.get("token") !== undefined) {
            return props.children;
        } else if (Cookies.get("token") === undefined) {
            return <Navigate to={"/login"} />;
        }
    };

    const LoginRoute = (props) => {
        if (Cookies.get("token") !== undefined) {
            return <Navigate to={"/"} />;
        } else if (Cookies.get("token") === undefined) {
            return props.children;
        }
    };

    return (
        <div className="text-base">
            <BrowserRouter>
                <Routes>
                    <Route path="*" element={<NotFound />} />
                    <Route path="/" element={<Home />} />
                    <Route
                        path="/dashboard"
                        element={
                            <PrivateRoutes>
                                <Dashboard />
                            </PrivateRoutes>
                        }
                    />
                    <Route
                        path="/dashboard/user"
                        element={
                            <PrivateRoutes>
                                <User />
                            </PrivateRoutes>
                        }
                    />
                    <Route
                        path="/dashboard/list-job-vacancy"
                        element={
                            <PrivateRoutes>
                                <JobVacancy />
                            </PrivateRoutes>
                        }
                    />
                    <Route
                        path="/dashboard/list-job-vacancy/form"
                        element={<DataForm />}
                    />
                    <Route
                        path="/dashboard/profile"
                        element={
                            <PrivateRoutes>
                                <Profile />
                            </PrivateRoutes>
                        }
                    />

                    <Route
                        path="/login"
                        element={
                            <LoginRoute>
                                <Login />
                            </LoginRoute>
                        }
                    />

                    <Route path="/register" element={<Register />} />
                    <Route
                        path="/dashboard/change-password"
                        element={
                            <PrivateRoutes>
                                <ChangePassword />
                            </PrivateRoutes>
                        }
                    />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
