import React from "react";
import DashboardLayout from "../../layout/dashboard/index";

const Profile = () => {
    return (
        <DashboardLayout>
            <div>Profile</div>
        </DashboardLayout>
    );
};

export default Profile;
