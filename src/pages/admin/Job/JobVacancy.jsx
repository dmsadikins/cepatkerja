import React, { useEffect, useState } from "react";
import DashboardLayout from "../../../layout/dashboard/index";
import { Link } from "react-router-dom";
import axios from "axios";

const JobVacancy = () => {
    const [data, setData] = useState(null);
    const [fetchStatus, setFetchStatus] = useState(true);

    useEffect(() => {
        if (fetchStatus == true) {
            axios
                .get("https://dev-example.sanbercloud.com/api/job-vacancy")
                .then((res) => {
                    setData([...res.data.data]);
                })
                .catch((error) => {
                    alert(error.message);
                });
        }

        setFetchStatus(false);
    }, [fetchStatus, setFetchStatus]);

    console.log(data);
    return (
        <DashboardLayout>
            <table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Job Vacancy</th>
                        <th>Company</th>
                        <th>City</th>
                        <th>Tenure</th>
                        <th>Salary</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {data !== null &&
                        data.map((job, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>
                                    <Link to={`${job.id}`}>{job.title}</Link>
                                </td>
                                <td>{job.company_name}</td>
                                <td>{job.company_city}</td>
                                <td>{job.job_tenure}</td>
                                <td>
                                    {job.salary_min} - {job.salary_max}
                                </td>
                                <td>
                                    <ul>
                                        <li>Edit</li>
                                        <li>Delete</li>
                                    </ul>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>
        </DashboardLayout>
    );
};

export default JobVacancy;
