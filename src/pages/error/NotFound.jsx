import React from "react";
import Button from "../../components/Button";
import AuthLayout from "../../layout/authentication/Index";
import { Link, useNavigate } from "react-router-dom";

const NotFound = () => {
    const navigate = useNavigate();

    const handleClick = () => navigate(-1);

    return (
        <AuthLayout>
            <div className="w-[500px] bg-transparent shadow-custom rounded-lg flex-column justify-center text-center">
                <h1 className="text-dark font-bold text-5xl mb-3 py-5">Oops</h1>
                <div className="text-gray-500 text-3xl font-semibold mb-10 ">
                    We can't find that page.
                </div>

                <Link
                    to="/"
                    className=" py-3 px-6 bg-primary text-white rounded font-semibold hover:bg-primary-active"
                    onClick={handleClick}
                >
                    Go back
                </Link>
                <img
                    src="/404-error-dark.png"
                    className="w-full max-h-[800px]"
                    alt=""
                />
            </div>

            <section className="w-[500px] pt-8 text-muted text-center">
                <p>@2023 Cepat Kerja.com. All rights reserved.</p>
            </section>
        </AuthLayout>
    );
};

export default NotFound;
