import React from "react";
import Button from "../../components/Button";
import TextInput from "../../components/TextInput";
import DashboardLayout from "../../layout/dashboard/index";

const ChangePassword = () => {
    return (
        <DashboardLayout>
            <div className="w-300 md:w-[800px]  mt-7 rounded-md">
                <form className="w-100">
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="password"
                            label="Current Password"
                            name="current_password"
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="password"
                            label="New Password"
                            name="new_password"
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="password"
                            label="Confirm New Password"
                            name="new_confirm_password"
                        />
                    </div>
                    <div className="mb-10 hover:opacity-80 font-semibold">
                        <Button type="submit" bg="bg-primary">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
        </DashboardLayout>
    );
};

export default ChangePassword;
