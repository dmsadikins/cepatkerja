import React, { useState } from "react";
import Button from "../../components/Button";
import TextInput from "../../components/TextInput";
import AuthLayout from "../../layout/authentication/Index";
import Cookies from "js-cookie";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

const Login = () => {
    let navigate = useNavigate();

    const [input, setInput] = useState({
        email: "",
        password: "",
    });

    console.log(input);
    const handleInput = (e) => {
        let value = e.target.value;
        let name = e.target.name;

        setInput({ ...input, [name]: value });
    };

    const handleLogin = (e) => {
        e.preventDefault();

        let { email, password } = input;
        // console.log(input);

        axios
            .post("https://dev-example.sanbercloud.com/api/login", {
                email,
                password,
            })
            .then((res) => {
                console.log(res);
                let data = res.data;
                Cookies.set("token", data.token, { expires: 1 });
                navigate("/");
                Swal.fire({
                    text: "Successfull Loggedin",
                    icon: "success",
                    timer: 2000,
                    confirmButtonText: "Ok, let go!",
                });
            })
            .catch((error) => {
                Swal.fire({
                    title: error.message,
                    text: Object.values(error.response.data),
                    icon: "error",
                    confirmButtonText: "Ok",
                });
            });
    };
    return (
        <AuthLayout>
            <div className="w-full md:w-[500px] bg-white p-10 rounded-md pb-20">
                <form className="w-100" onSubmit={handleLogin}>
                    <div className="flex justify-center mb-2">
                        <img src="/vite.svg" alt="logo" width="50" />
                    </div>
                    <div className="text-center mb-11">
                        <h1 className="text-dark font-bold mb-3">Sign In</h1>
                        <div className="text-gray-500 font-semibold">
                            Welcome to Cepat Kerja.com
                        </div>
                    </div>
                    <div className="mb-8 gap-3 relative">
                        <TextInput
                            placeholder={"Email"}
                            name={"email"}
                            value={input.email}
                            onChange={handleInput}
                            type={"email"}
                            label={"Email"}
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            placeholder={"Password"}
                            name={"password"}
                            value={input.password}
                            onChange={handleInput}
                            type={"password"}
                            label={"Password"}
                        />
                        <div className="change_password text-end text-primary mt-3 cursor-pointer">
                            <Link to="/dashboard/change-password">
                                Change Password
                            </Link>
                        </div>
                    </div>
                    <div className="mb-10 hover:opacity-80 font-semibold">
                        <Button type="submit" bg="bg-primary" size="w-full">
                            Sign In
                        </Button>
                    </div>

                    <div className="flex justify-center text-muted font-semibold">
                        <span>Not a member yet?</span>
                        <Link
                            to="/register"
                            className="!text-primary underline pl-2"
                        >
                            Sign up
                        </Link>
                    </div>
                </form>
            </div>
            <section className="w-full md:w-[500px] pt-8 text-muted text-center">
                <p>@2023 Cepat Kerja.com. All rights reserved.</p>
            </section>
        </AuthLayout>
    );
};

export default Login;
