import React, { useState } from "react";
import AuthLayout from "../../layout/authentication/Index";
import Button from "../../components/Button";
import TextInput from "../../components/TextInput";
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import axios from "axios";
import Swal from "sweetalert2";

const Register = () => {
    const navigate = useNavigate();

    const [input, setInput] = useState({
        name: "",
        image_url: "",
        email: "",
        password: "",
    });

    const handleInput = (e) => {
        let value = e.target.value;
        let name = e.target.name;

        setInput({ ...input, [name]: value });
    };

    const handleRegister = (e) => {
        e.preventDefault();

        const { name, image_url, email, password } = input;

        console.log(input);

        axios
            .post("https://dev-example.sanbercloud.com/api/register", {
                name,
                image_url,
                email,
                password,
            })
            .then((res) => {
                console.log("res");
                let data = res.data;
                Cookies.set("token", data.token, { expires: 1 });
                navigate("/");
                Swal.fire({
                    text: "Your account has been successfully created",
                    icon: "success",
                    timer: 2000,
                    confirmButtonText: "Continue",
                });
            })
            .catch((error) => {
                // alert(error.message);
                Swal.fire({
                    text: error.message,
                    icon: "error",
                    confirmButtonText: "OK, go",
                });
            });
    };
    return (
        <AuthLayout>
            <div className="w-full md:w-[500px] bg-white   p-10 rounded-md">
                <form className="w-100" onSubmit={handleRegister}>
                    <div className="flex justify-center mb-2">
                        <img src="/vite.svg" alt="logo" width="50" />
                    </div>
                    <div className="text-center mb-11">
                        <h1 className="text-dark font-bold mb-3">Sign Up</h1>
                        <div className="text-gray-500 font-semibold">
                            Welcome to Cepat Kerja.com
                        </div>
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="text"
                            name="name"
                            value={input.name}
                            onChange={handleInput}
                            label={"Name"}
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="text"
                            label="Image Link"
                            name="image_url"
                            value={input.image_url}
                            onChange={handleInput}
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="email"
                            label="Email"
                            name="email"
                            value={input.email}
                            onChange={handleInput}
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="password"
                            label="Password"
                            name="password"
                            value={input.password}
                            onChange={handleInput}
                            required
                        />
                    </div>
                    <div className="mb-8 gap-3">
                        <TextInput
                            type="password"
                            label="Repeat Password"
                            name="password_confirmation"
                        />
                    </div>
                    <div className="mb-10 hover:opacity-80 font-semibold">
                        <Button type="submit" bg="bg-primary" size="w-full">
                            Sign up
                        </Button>
                    </div>

                    <div className="flex justify-center text-muted font-semibold">
                        <span>Already have an Account?</span>
                        <Link
                            to="/login"
                            className="!text-primary underline pl-2"
                        >
                            Sign in
                        </Link>
                    </div>
                </form>
            </div>
            <section className="w-full md:w-[500px] pt-8 text-muted text-center">
                <p>@2023 Cepat Kerja.com. All rights reserved.</p>
            </section>
        </AuthLayout>
    );
};

export default Register;
