import React from "react";
import FrontendLayout from "../../layout/frontend";
const Home = () => {
    return (
        <FrontendLayout>
            <section className="w-full px-3 md:px-0">
                <div className="text-center">
                    <h3 className="text-3xl font-bold">Browse Categories</h3>
                    <h5 className="text-muted mt-2">
                        Most popular categories of portal sorted by popularity
                    </h5>
                </div>
            </section>
        </FrontendLayout>
    );
};

export default Home;
